README
======

This update addresses the following 
-----------------------------------

1. Fixes VLC player (offline installation)

Build
-----

Clean the cache directory and download the vlc and its dependencies ::

        sudo apt clean; sudo apt purge vlc; sudo apt install --download-only vlc

Now, copy the deb files to ``payload`` directory ::
        
        cp -v /var/cache/apt/archives/*.deb payload/

Now generate the shell-archive, it is generated using a ``makeself`` utility:: 
    
        makeself --noprogress --target /tmp/payload  payload update-3-offline.run "update 3 offline" /tmp/payload/update-3-offline.sh
