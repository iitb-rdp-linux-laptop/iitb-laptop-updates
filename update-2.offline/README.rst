README
======

This is an offline version of the ``update-2``, if you have already applied ``update-2`` then you can skip this
repository. This offline update file can be downloaded on a pendrive and then can be copied to compatible laptop 
to update.

This update addresses the following 
-----------------------------------

1. Add support to audio jack detection (Automatically switch between a headphone jack and speakers)
#. Add support to microphone
#. Provides a clear audio through headphones and speakers
#. Improves Bluetooth stability


Known issues
------------

1. The wireless signal strength on the system tray appear low, however, the reception and connectivity remain good.
   This issue will not affect your WiFi performance. We will fix this minor issue with a patch soon.

Build
-----

The update-2-offline.run file is a shell-archive, it is generated using a ``makeself`` utility:: 
    
        wget -c https://ld.iitb.ac.in/debian/linux-image-4.19.0-rc7-iitb_amd64.deb -O payload/linux-image-4.19.0-rc7-iitb_amd64.deb && makeself --noprogress --target /tmp/payload  payload update-2-offline.run "update 2 offline" /tmp/payload/update-2-offline.sh
